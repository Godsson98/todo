package todo.commonClasses;

public enum PriorityToDo {
	High,
	Medium,
	Low;
}
