package todo.commonClasses.fileManager;

import todo.appClasses.Task_dueDate;

public class ToDoFileManager extends FileManager {


	@Override
	public String getPropertySeparator() {
		return ";";
	}
	
	public ToDoFileManager() {
		super("ToDo.txt");
	}

	@Override
	protected Task_dueDate fromProperties(String[] properties) {
		Task_dueDate task = null;
		if(properties.length == 5) {
			task = new Task_dueDate(properties[1],
					properties[2],
					properties[3],
					properties[4]);
			task.setId(properties[0]);
		}
		return task;
	}

}
