# Project - ToDo

## Team
Daniel Gergely
Tufan Basaran

## Table of contents
* [General info](#general-info)
* [Technologies](#technologies)
* [Functionalities](#technologies)
* [Setup](#setup)

## General info
The Server...

- is deployed on localhost, port 50002. 
- has an API which is a simple text based messaging
- can service multiple clients while running
- has no GUI, is a console application
- can be found and run @ todo -> commonClasses -> server -> server.java

The Client...
- has been implemented using the Java_App template
- has a responsive GUI
- communicates with the server using simple text based messaging
- can be found and run @ todo -> ToDo.java
	
The main functionalities are, among others, the ability to register and login to the program, create, view and delete various ToDo entries. Data is stored in a text files only accessible by the server. This enables users to login and access their ToDos even after a shutdown of the program. The functionalities will be explained in detail further in the document.
	
## Technologies
Project is created with:
* Eclipse version: 2020-09

## Functionalities
The functionalities are split into following categories: basic concept, general functionality and optional functionality.

### Basic Concept
The basic concept is a server which is reachable on a particular IP Address and the Port 50002. 
This server is able to serve multiple clients at the same time. Also no business logic is on the client side. The clients can only send requests, and the server validates these messages and decides what to answer.


### General functionality
A User is able to login and Create, View, or Delete a ToDo.

#### Account management

The account management functionalities contain:

- Create account
- Login
- Change password
- Logout

#### TODO List

The ToDo List functionalities contain: 

- Create ToDo
- View all ToDos in a list
- Delete ToDo

Note: Existing ToDo entries cannot be changed, only deleted. (As stated in the requirements)

#### Server
- Is able to serve multiple clients in parallel (like a web server)
- The server listens on port 50002 – no input, no choice
- Passwords are not encrypted; they are stored and compared in plain text
- Sends a token after a successful login which needs to be sent by every request afterwards
- These tokens are stored with the userIds in a map. When user Id is needed, the token is sent and the Id is returned. (At logout token and Id are removed from map)
- Only implement ToDos without due-dates (We didn't have time to implement due_dates, because of other projects)
- Data (ToDos and Users) is saved in text files and automatically read in to the program at each server start

#### Client
- Connects and disconnects from server at every request
- After login sends token received from server for identification purposes
- Has no access to data, only messages sent by the server. 
- Does basic validation defined in the requirements (username = email, password length min 3 characters)
- ToDo titles are stored in a ListView for a better overview.
- Server replies displayed in a scroll bar in the GUI.
- A Ping can always be sent through the menu
- Signing out is also possible through the menu. Token will also be deleted. To continue a new login is needed
- User profile can also be accessed through the menu

### Optional functionalities

- Username (Email) and password validation - client side
- MVC - client and server side
- GUI -client side
	- Splash screen
	- Tooltip
	- Translator
	- Tasks stored in list view
	- Pop-up windows (Todos and Profile)
	
- Data Storage using file managers for user and todos - server side
- logging both server and client side
- Each user only sees the ToDos he/she created



## Setup
To run this project, please start first the server (todo -> commonClasses -> server -> server.java) and then the client (todo -> ToDo.java).

In the Users.txt and the ToDo.txt there is already some dummy data present (for better testing purposes). 

The two users present are: 

username: test1@test.com password: Test123

username: test2@test.com password: Test123

## Notes
A note to the client side list view. The fillList method in the ClientController (line 311) fills the listView with all the todos of the user. See comments for more detail.

Due dates for todos were not implemented due to time issues. We had other projects going on and just simply didn't have enough time despite working 10-14 hours daily on the project the last couple of days.